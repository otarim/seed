'use strict'
var koa = require('koa'),
	fs = require('mz/fs'),
	path = require('path'),
	staticServe = require('koa-static-cache'),
	router = require('koa-router')(),
	koaBody = require('koa-body'),
	redis = require('redis'),
	redisStore = require('koa-redis'),
	session = require('koa-generic-session'),
	co = require('co'),
	swig = require('koa-swig'),
	Memcached = require('co-memcached'),
	config = require('./config/config'),
	mongo = require('./config/db'),
	controllers = require('./controllers/router'),
	captcha = require('koa-captcha'),
	jwt = require('koa-jwt'),
	app = koa()

if (config.env === 'production') {
	app.use(require('koa-compress')({
		threshold: 256,
		flush: require('zlib').Z_SYNC_FLUSH
	}))
	app.use(require('koa-etag')())
	app.use(require('koa-fresh')())
	app.use(require('koa-html-minifier')({
			collapseWhitespace: true,
			removeComments: true,
			minifyCSS: true,
			minifyJS: {
				compress: {
					drop_console: true
				}
			}
		})) //should behind compress
} else {
	app.use(require('koa-livereload')())
}
app.keys = config.sessionKeys
app.use(session({
	store: redisStore()
}))

// app.use(jwt({
// 	secret: config.jwtSecret,
// 	passthrough: true
// }))

// app.use(captcha({
// 	url: '/captcha.jpg',
// 	length: 4,
// 	fontSize: 3,
// 	width: 100,
// 	height: 50
// }))

app.use(require('koa-favicon')(__dirname + '/static/favicon.png'))

if (config.env !== 'production') {
	app.use(require('koa-error')())
	app.use(require('koa-logger')())
}

app.use(staticServe('./static/', {
	maxAge: config.env === 'production' ? 24 * 60 * 60 : 0
}))

app.use(koaBody({
	multipart: true,
	formidable: {
		keepExtensions: true,
		hash: 'sha1'
	}
}))

app.context.render = swig({
	root: path.join(__dirname, 'views'),
	autoescape: true,
	cache: config.env === 'production' ? 'memory' : false,
	ext: 'swig',
	// varControls: ['[[', ']]']
    // locals: locals,
    // filters: filters,
    // tags: tags,
    // extensions: extensions
})

// mc
// app.context.mc = new Memcached('127.0.0.1:11211')

app.init = co.wrap(function*() {
	// redis
	app.context.redis = require('co-redis')(redis.createClient())
		// db
	app.context.db = yield mongo.connect()
		// router
	app.use(router.routes()).use(router.allowedMethods())
	yield controllers(router)
	app.listen(config.port)
})

app.init().catch(function(err) {
	console.log(err.stack)
})