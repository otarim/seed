'use strict'
var _ = require('lodash')

var config = {
	production: {
		db: 'mongodb://127.0.0.1:27017/app'
	},
	development: {
		db: 'mongodb://127.0.0.1:27017/app'
	}
}

module.exports = _.assign({}, config[process.env.NODE_ENV || 'development'], {
	env: process.env.NODE_ENV || 'development',
	port: 3000,
	sessionKeys: ['sessionKey'],
	jwtSecret: 'misaka'
})